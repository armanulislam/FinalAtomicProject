-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 30, 2016 at 09:11 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomicprojectb22`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `birthday` date NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthday`, `deleted_at`) VALUES
(1, 'arman', '0000-00-00', 1467263457),
(2, 'arman', '0000-00-00', NULL),
(3, '', '2536-02-25', NULL),
(4, 'dddd5', '0000-00-00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `deleted_at` int(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `title`, `deleted_at`) VALUES
(6, 'bangladesh economics', 1467269539),
(9, 'guiui', NULL),
(10, 'ereret', NULL),
(11, 'ereret', NULL),
(12, 'ereret', NULL),
(13, 'hhhhh', NULL),
(14, 'ddddddddddddddddd', NULL),
(15, 'asdf', NULL),
(16, '', NULL),
(17, 'hhhhh', NULL),
(18, 'hhhhh', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `city` varchar(250) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `deleted_at`) VALUES
(1, '999', 'Sylhet', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE IF NOT EXISTS `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `level` varchar(20) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `name`, `level`, `deleted_at`) VALUES
(1, 'arman', 'DIPLOMA-Engineering', NULL),
(3, 'nahidul hasan', 'DIPLOMA-Engineering', NULL),
(4, 'd', 'SSC', NULL),
(5, 'ddfdf', 'BSC in ETE', NULL),
(6, 'vfvfvfvb', 'HSC', NULL),
(7, 'dsfasfsdfsd', 'BSC in CSE', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) NOT NULL,
  `deleted_at` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `email`, `deleted_at`) VALUES
(1, 'arman....1545......@gmail.com', NULL),
(7, 'arman.p@gmail.com', NULL),
(8, 'fb@g', NULL),
(9, 'dddd', NULL),
(10, 'fdfdf', NULL),
(12, 'hello@gmail.com', NULL),
(13, 'arman.pciu@gmail.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE IF NOT EXISTS `hobby` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hobby` varchar(250) NOT NULL,
  `deleted_at` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `hobby`, `deleted_at`) VALUES
(1, 'cricket,football', NULL),
(40, 'cricket,football', NULL),
(42, 'Football,Hiking,Travelling', NULL),
(43, 'Gardening,Playing Football,Coding', NULL),
(44, 'Gardening,Playing Football', NULL),
(45, 'Gardening', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `summary` varchar(500) NOT NULL,
  `deleted_at` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `name`, `summary`, `deleted_at`) VALUES
(1, 'unicef', 'hello111111111111', NULL),
(4, 'ddddddkkkk', 'ddddd', NULL),
(5, 'fddf', 'fdfdfdsf', NULL),
(6, 'jjhjjhjh', 'iiiiiihhihi', NULL),
(8, 'fff', 'fggr', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE IF NOT EXISTS `profilepicture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `images` varchar(250) NOT NULL,
  `deleted_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `images`, `deleted_at`) VALUES
(1, 'arman', '1467268505images.png', NULL),
(2, 'armanul islam', '1467268475images (1).png', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
