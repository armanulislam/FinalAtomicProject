<?php


namespace App\BITM\SEIP137028\Education;


include_once('../../../vendor/autoload.php');

use App\BITM\SEIP137028\Message;
use App\BITM\SEIP137028\Utility;

class Education
{
    public $id;
    public $level;
    public $name;
    public $deleted_At;
    public $conn;
    public $alllevelData = array();
    public $allTrashedlevelData = array();

    function __construct()
    {
        $this->conn = mysqli_connect('localhost', 'root', "", 'atomicprojectb22');
    }

    // Accessing DB for list to show data
   


    public function prepareVariableValue($data)
    {
        if (array_key_exists('level', $data))
            $this->level = $data['level'];


        if (array_key_exists('name', $data))
            $this->name = $data['name'];

        if (array_key_exists('id', $data))
            $this->id = $data['id'];
    }
    function index()
    {
        $sql = "SELECT * FROM `education` where deleted_at is null";
        $result = mysqli_query($this->conn, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in alllevelData variable and return it to show the data
        // in index.php file
// we must declare alllevelData as array to contain all data
        while ($row = mysqli_fetch_object($result))
            $this->alllevelData[] = $row;

        return $this->alllevelData;
    }

    function store()
    {

        $sql = "INSERT INTO `atomicprojectb22`.`education` (`name`, `level`)
 VALUES ('".$this->name."', '".$this->level."')";

        $result = mysqli_query($this->conn, $sql);


        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been stored successfully!</strong>
            </div>");

            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has not been stored successfully!</strong>
            </div>");


            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

        //echo $result;
        // echo $this->level;

        //return " I am storing data ";
    }


    function update()
    {
        $query = "UPDATE `education` SET `name` = '" . $this->name . "', `level` = '" . $this->level . "'
        WHERE `education`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been Updated successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function delete()
    {
        $query = "DELETE FROM `atomicprojectb22`.`education` WHERE `education`.`id` =" . $this->id;

        $result = mysqli_query($this->conn, $query);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been deleted successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function view()
    {
        $query = "SELECT * FROM `education` WHERE `id`=" . $this->id;
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }

    function trash()
    {
        $this->deletedAt = time();

        $sql = "UPDATE `education` SET `deleted_at` = '".$this->deleted_At."' WHERE `education`.`id` =". $this->id;
        $result = mysqli_query($this->conn, $sql);

        if ($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been trashed successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function trashed()
    {
        $sql = "SELECT * FROM `education` where deleted_at is not null";
        $result = mysqli_query($this->conn, $sql);

        // After getting result we need to contain those data in a variable
        // saving fetching data in alllevelData variable and return it to show the data
        // in index.php file
// we must declare alllevelData as array to contain all data
        while ($row = mysqli_fetch_assoc($result))
            $this->allTrashedlevelData[] = $row;

        return $this->allTrashedlevelData;
    }

    function recover ()
    {
        $sql = "update education set deleted_at = null where id = ". $this->id;
        $result = mysqli_query($this->conn, $sql);

        if($result) {
            Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Data has been recovered successfully!</strong>
            </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else Message::examineMessage("error");
    }

    function recoverSelected($ids = array())
    {
        if(is_array($ids) && count($ids) > 0) {
            $IDs = implode(",", $ids);

            $sql = "update education set deleted_at = NULL WHERE id IN (". $IDs .")";
            $result = mysqli_query($this->conn, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been recovered successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    function deleteSelected($IDs = array())
    {
        if(is_array($IDs) && count($IDs) > 0) {
            $ids = implode(",", $IDs);

            $sql = 'delete from `education` where id IN ('.$ids.')';

            $result = mysqli_query($this->conn, $sql);

            if($result) {
                Message::examineMessage("
            <div class=\"alert alert-success\">
            <strong>Selected Data has been Deleted successfully!</strong>
            </div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else Message::examineMessage("error");
        }
    }

    public function count(){
        $query="SELECT COUNT(*) AS totalItem FROM `education` WHERE deleted_at IS NULL";
        $result=mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row['totalItem'];
    }
    public function paginator($pageStartFrom=0,$Limit=5){
        $_alllevel = array();
        $query="SELECT * FROM `education` WHERE deleted_at is null LIMIT ".$pageStartFrom.",".$Limit;
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $_alllevel[] = $row;
        }

        return $_alllevel;

    }
}