<?php
namespace App\BITM\SEIP137028\Email;
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\Message;

class Email{
    public $id="";
    public $email="";
    public $deleted_at="";
    public $conn;

        public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }
    public function prepare($data=""){
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`email` (`email`) VALUES ('".$this->email."')";
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            echo "Error";
        }
    }
    public function index()
    {
        $_allemail=array();
       $query="SELECT * FROM `email` where `deleted_at` is null";
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result)){
            $_allemail[]=$row;
        }
        return $_allemail;
    }
    public function view(){
        $query="SELECT * FROM `email` where `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;
    }
    public function edit(){
        $query="UPDATE `atomicprojectb22`.`email` SET `email` = '".$this->email."' WHERE `email`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            echo "Error";
        }

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`email` WHERE `email`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
    }
    public function trash(){
        $this->deleted_at = time();
        $query="UPDATE `atomicprojectb22`.`email` SET `deleted_at` = '".$this->deleted_at."' WHERE `email`.`id` =" .$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trash successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
    }
    public function trashed(){
        $_allemail=array();
        $query="SELECT * FROM `email` where `deleted_at` is not null";
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result)){
            $_allemail[]=$row;
        }
        return $_allemail;
    }
    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`email` SET `deleted_at` = NULL WHERE `email`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

    }
    public function recoverSeleted($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`email` SET `deleted_at` = NULL WHERE `email`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else {
                Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            }

        }

    }
    public function deleteMultiple($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "DELETE FROM `atomicprojectb22`.`email` WHERE `email`.`id`  IN(" . $ids . ")";
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been deleted successfully.
</div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else {
                Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been deleted successfully.
</div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            }

        }
    }
    

}