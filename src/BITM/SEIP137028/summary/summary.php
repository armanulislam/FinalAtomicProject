<?php
namespace App\BITM\SEIP137028\summary;
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\Message;
class summary{
    public  $id="";
    public $org_name="";
    public $summary="";
    public $conn;
    public function __construct()
    {
        $this->conn = mysqli_connect("localhost", "root", "", "atomicprojectb22") or die("Database connection failed");
    }
    public function prepare($data=""){
        if(array_key_exists('name',$data)){
            $this->org_name=$data['name'];
        }
        if(array_key_exists('summary',$data)){
            $this->summary=$data['summary'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
    }
    public function store(){
        $query="INSERT INTO `atomicprojectb22`.`organization` (`name`, `summary`) VALUES ('".$this->org_name."', '".$this->summary."')";
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully.
                </div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            echo "Error";
        }
    }
    public function index()
    {
        $allsummary = array();
        $query = "SELECT * FROM `organization` WHERE `deleted_at` IS NULL";
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_object($result)) {
            $allsummary[] = $row;
        }

        return $allsummary;
    }
    public function view(){
        $query="SELECT * FROM `organization` where `id`=".$this->id;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_object($result);
        return $row;
    }
    public function edit(){
        $query="UPDATE `atomicprojectb22`.`organization` SET `name` = '".$this->org_name."', `summary` = '".$this->summary."' 
                WHERE `organization`.`id` = ".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Success!</strong> Data has been updated  successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            echo "Error";
        }

    }
    public function delete(){
        $query="DELETE FROM `atomicprojectb22`.`organization` WHERE `organization`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been deleted successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
    }
    public function trash(){
       $this->deleted_at=time();
        $query="UPDATE  `atomicprojectb22`.`organization` SET  `deleted_at` =  '".$this->deteted_at."' WHERE  `organization`.`id` =".$this->id;
        $result=mysqli_query($this->conn,$query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been trash successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been trashed successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }
    
    }
    public function trashed(){
        $allsummary=array();
        $query="SELECT * FROM `organization` where `deleted_at` is not null";
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_object($result)){
            $allsummary[]=$row;
        }
        return $allsummary;
    }
    public function recover()
    {

        $query = "UPDATE `atomicprojectb22`.`organization` SET `deleted_at` = NULL WHERE `organization`.`id` = " . $this->id;
        $result = mysqli_query($this->conn, $query);
        if ($result) {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has been recovered successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        } else {
            Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Data has not been recovered successfully.
</div>");
            Utility::reDirectAPageIntoAnotherPage("index.php");
        }

    }
    public function recovered($IDs = Array())
    {
        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $query = "UPDATE `atomicprojectb22`.`organization` SET `deleted_at` = NULL WHERE `organization`.`id` IN(" . $ids . ")";
            //echo $query;
            //die();
            $result = mysqli_query($this->conn, $query);
            if ($result) {
                Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has been recovered successfully.
</div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            } else {
                Message::examineMessage("
<div class=\"alert alert-info\">
  <strong>Deleted!</strong> Selected Data has not been recovered successfully.
</div>");
                Utility::reDirectAPageIntoAnotherPage("index.php");
            }

        }

    }

}
?>
