<?php
session_start();
include_once("../../../vendor/autoload.php");
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\Message;


$birth= new \App\BITM\SEIP137028\Birth\Birth();

//Utility::d($allBook);

if(array_key_exists('itemPerPage',$_SESSION)) {
    if (array_key_exists('itemPerPage', $_GET)) {
        $_SESSION['itemPerPage'] = $_GET['itemPerPage'];
    }
}
else{
    $_SESSION['itemPerPage']=5;
}

//Utility::dd($_SESSION['itemPerPage']);

$itemPerPage=$_SESSION['itemPerPage'];
$totalItem=$birth->count();



$totalPage=ceil($totalItem/$itemPerPage);
//Utility::dd($itemPerPage);
$pagination="";


if(array_key_exists('pageNumber',$_GET)){
    $pageNumber=$_GET['pageNumber'];
}else{
    $pageNumber=1;
}
for($i=1;$i<=$totalPage;$i++){
    $class=($pageNumber==$i)?"active":"";
    $pagination.="<li class='$class'><a href='index.php?pageNumber=$i'>$i</a></li>";
}

$pageStartFrom=$itemPerPage*($pageNumber-1);
$getAllBirthData=$birth->paginator($pageStartFrom,$itemPerPage);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Birth Date</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>User Birth Day List</h2>
    <a href="create.php" class="btn btn-info" role="button">Create Again</a>
    <a href="trashed.php" class="btn btn-info" role="button">Go to trashed</a>
    <div id = 'message'>
        <?php
        if (array_key_exists('message', $_SESSION) && (!empty($_SESSION['message'])))
            echo Message::examineMessage();
        ?>
    </div>
    <form role="form">
        <div class="form-group">
            <label for="sel1">Select homw many items you want to show (select one):</label>
            <select class="form-control" id="sel1" name="itemPerPage">
                <option

                    <?php
                    if ($itemPerPage == 5) {

                        ?>
                        selected

                        <?php
                    }
                    ?>


                >5</option>
                <option
                    <?php
                    if ($itemPerPage == 10) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >10</option>
                <option
                    <?php
                    if ($itemPerPage == 15) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >15</option>
                <option
                    <?php
                    if ($itemPerPage == 20) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >20</option>
                <option
                    <?php
                    if ($itemPerPage == 25) {

                        ?>
                        selected

                        <?php
                    }
                    ?>
                >25</option>
            </select>
            <button type="submit">Go!</button>

        </div>
    </form>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>name</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <!--        Here getAllBookData is a object -->
        <?php
        $serialNumber = 1;
        foreach ($getAllBirthData as $birth) {
            $date = "";
            ?>
            <tr>
                <td><?php echo $serialNumber++  + $pageStartFrom ?></td>
                <td><?php echo $birth['id'] ?></td>
                <td><?php echo $birth['name'] ?></td>

                <?php
                $contain = explode("-", $birth['date']);

                $date = $contain[2]. "-" . $contain[1] . "-" . $contain[0];

                ?>

                <td><?php echo $date ?></td>
                <td>
                    <a href="view.php?id=<?php echo $birth['id']?>" class="btn btn-info" role="button">View</a>
                    <a href="edit.php?id=<?php echo $birth['id']?>" class="btn btn-primary" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $birth['id']?>" class="btn btn-danger" role="button">Delete</a>
                    <a href="trash.php?id=<?php echo $birth['id']?>" class="btn btn-warning" role="button">Trash</a>
                </td>
            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
    <ul class="pagination">
        <li
            <?php
            if ($pageNumber == 1) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber - 1) ?>>">Prev</a></li>
        <?php echo $pagination ?>
        <li
            <?php
            if ($pageNumber == $totalPage) {

                ?>
                class="hidden"

                <?php
            }
            ?>
        ><a href="index.php?pageNumber= <?php echo($pageNumber + 1) ?>>">Next</a></li>
    </ul>
</div>

<script>

    $('#message').show().delay(2000).fadeOut();
</script>

</body>
</html>

