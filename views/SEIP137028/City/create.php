<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    for offline -->
    <link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js">
    <!--for online also works on ofline-->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Creating City and user</h2>

    <form class="form-horizontal" role="form" action="store.php" method="post">
        <div class="form-group">
            <label class="control-label col-sm-2">Enter name:</label>

            <div class="col-sm-10">
                <input type="text" class="form-control" name='name' id="name" placeholder="Enter name">
            </div>
        </div>

        <div class="form-horizontal">
            <label>City (Select one):</label>
            <select class="form-control" id="city" name="city">
                <option


                >Dhaka
                </option>
                <option

                >Chittagong
                </option>
                <option

                >Rajshahi
                </option>
                <option

                >Barishal
                </option>
                <option

                >Sylhet
                </option>
            </select>

        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>


<!--<head>-->
<!--<link rel="stylesheet" type="text/css" href="../../../resource/css/bootstrap.css">-->
<!--</head>-->
<!---->
<!--<head>-->
<!--    <link rel="stylesheet" type="text/css" href="../../../resource/js/bootstrap.js">-->
<!--</head>-->

