<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP137028\City\City;

$cityView = new \App\BITM\SEIP137028\City\City();
$cityView->prepareVariableValue($_GET);
$getSingleCityData = $cityView->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book Title</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $getSingleCityData->name ?></h2>

    <table class="table table-hover">
        <thead>
        <tr>

            <th>ID</th>
            <th>name</th>
            <th>City</th>

        </tr>
        </thead>
        <tbody>


        <tr>
            <td><?php echo $getSingleCityData->id ?></td>
            <td><?php echo $getSingleCityData->name ?></td>
            <td><?php echo $getSingleCityData->city ?></td>
        </tr>
        </tbody>
    </table>
</div>


</body>
</html>


