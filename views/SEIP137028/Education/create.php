<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

    <form role="form" method="post" action="store.php">
        <h2>Your Name</h2>
        <div class="form-group">

            <div class="col-sm-10">
                <input type="text" class="form-control" name='name' id="name" placeholder="Enter name">
            </div>
        </div>

        <br>

        <h2>Select your Level</h2>


        

        <div class="radio">
            <label><input type="radio" name="level" value="SSC"> SSC</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="HSC"> HSC</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="DIPLOMA-Engineering"> DIPLOMA-Engineering</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in CSE"> BSC in CSE</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in EEE"> BSC in EEE</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in ETE"> BSC in ETE</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in Civil"> BSC in Civil</label>
        </div>
        <div class="radio">
            <label><input type="radio" name="level" value="BSC in Architecture"> BSC in Architecture</label>
        </div>
        <input type="submit" value="Submit">
    </form>
</div>

</body>
</html>

