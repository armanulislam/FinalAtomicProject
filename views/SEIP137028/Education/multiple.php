<?php

include "../../../vendor/autoload.php";
use App\BITM\SEIP137028\Education\Education;

$education = new Education();



if(array_key_exists('recoverAll', $_POST)) {
    $education->recoverSelected($_POST['id']);
}


if(array_key_exists('deleteAll', $_POST)) {
    $education->deleteSelected($_POST['id']);
}