<?php

include "../../../vendor/autoload.php";
use App\BITM\SEIP137028\Education\Education;

$education = new Education();

$education->prepareVariableValue($_GET);
$education->recover();