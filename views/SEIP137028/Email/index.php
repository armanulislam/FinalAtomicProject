<?php
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP137028\Email\Email;
use App\BITM\SEIP137028\Message;
use App\BITM\SEIP137028\Utility;


$obj=new Email();
$obj->prepare($_GET);
$object=$obj->index();


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>subscribe email</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <style>
        .actions {
            text-align: center;
        }
    </style>
</head>
<body>

<div class="container">
    <div class="col-md-8 col-md-offset-2">

        <h1>All Emails</h1>
        <p>
            <a href="create.php" class="btn btn-warning">Create New Email</a>
            <a href="trashed.php" class="btn btn-warning" role="button">View Trashed list</a>
        </p>
        <?php if(!empty($message)) : ?>
            <div class="alert alert-success" role="alert">
                <?php echo $message;  ?>
            </div>
        <?php endif; ?>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>SL</th>
                <th>ID</th>
                <th>email</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($object as $item){
                $sl++; ?>
                <td><?php echo $sl?></td>
                <td><?php echo $item->id?></td>
                <td><?php echo $item->email?></td>
                <td><a href="view.php?id=<?php echo $item-> id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $item-> id ?>"  class="btn btn-info" role="button">Edit</a>
                    <a href="delete.php?id=<?php echo $item->id?>" class="btn btn-danger" role="button" id="delete"  Onclick="return ConfirmDelete()">Delete</a>
                    <a href="trash.php?id=<?php echo $item->id ?>"  class="btn btn-info" role="button">Trash</a>
                </td>

                </tr>
            <?php }?>
            </tbody>
        </table>

    </div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    //        $(document).ready(function(){
    //            $("#delete").click(function(){
    //                if (!confirm("Do you want to delete")){
    //                    return false;
    //                }
    //            });
    //        });
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
</body>
</html>
