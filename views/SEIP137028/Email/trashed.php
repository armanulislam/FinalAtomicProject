<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP137028\Email\Email;
use App\BITM\SEIP137028\Utility;
use App\BITM\SEIP137028\Message;

$trashed= new Email();
$alltrashed=$trashed->trashed();
//Utility::d($allBook);

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

</head>
<body>

<div class="container">
    <center><h2>All Trashed List</h2></center>
    <form action="multiplerecover.php" method="post" id="multiple">
        <a href="index.php" class="btn btn-success" role="button">Home</a>
        <a href="create.php" class="btn btn-warning" role="button">create New email</a>
        <button type="submit"  class="btn btn-warning">Recover Selected</button>
        <button type="button"  class="btn btn-warning" id="multiple_delete">Delete Selected</button>
        <div class="table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th>Check Item</th>
                    <th>#</th>
                    <th>ID</th>
                    <th>Emails</th>
                    <th>Action</th>

                </tr>
                </thead>
                <tbody>
                <tr>
                    <?php
                    $sl=0;
                    foreach($alltrashed as $email){
                    $sl++; ?>
                    <td><input type="checkbox" name="mark[]" value="<?php echo $email->id?>"></td>
                    <td><?php echo $sl?></td>
                    <td><?php echo $email-> id?></td>
                    <td><?php echo $email->email?></td>
                    <td><a href="recover.php?id=<?php echo $email-> id ?>" class="btn btn-primary" role="button">Recover</a>
                        <a href="delete.php?id=<?php echo $email->id?>" class="btn btn-danger" role="button" id="delete">Delete</a>

                    </td>

                </tr>
                <?php }?>


                </tbody>
            </table>
    </form>
</div>
</div>
<script>
    $('#message').show().delay(2000).fadeOut();


    $(document).ready(function(){
        $("#delete").click(function(){
            if (!confirm("Do you want to delete")){
                return false;
            }
        });
    });

    $('#multiple_delete').on('click',function(){
        document.forms[0].action="deletemultiple.php";
        $('#multiple').submit();
    });

</script>

</body>
</html>

