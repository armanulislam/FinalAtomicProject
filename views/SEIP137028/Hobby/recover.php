<?php

include "../../../vendor/autoload.php";
use App\BITM\SEIP137028\Hobby\Hobby;

$hobby = new Hobby();

$hobby->prepare($_GET);
$hobby->recover();