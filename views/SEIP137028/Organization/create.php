<!DOCTYPE html>
<html lang="en">
<head>
    <title>Organization summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <br>
    <center><a href="index.php" class="btn btn-success" role="button">Home</a>
        <a href="trashed.php" class="btn btn-warning" role="button">View Trashed list</a>
    </center>
    <h2>create Organization summary</h2>
    <form role="form" method="post" action="store.php">
        <div class="form-group">
            <label>Organization name:</label>
            <input type="text" name="name" class="form-control"  placeholder="Enter Organization names">
            <label for="comment">summary:</label>
            <textarea class="form-control" name="summary" rows="2" placeholder="enter new organization summary"></textarea>
        </div>
        <button type="reset" class="btn btn-warning">reset</button>
        <button type="Submit" class="btn btn-success">Submit</button>

    </form>
</div>

</body>
</html>

