<?php
//var_dump($_GET);
include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP137028\Book\Book;
use App\BITM\SEIP137028\Utility;

$book= new Book();
$book->prepare($_GET);
$singleItem=$book->view();
//Utility::d($singleItem);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Books titles</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2><center><?php echo $singleItem->title?></center></h2>
  <ul class="list-group">
    <li class="list-group-item">ID: <?php echo $singleItem->id?></li>
    <li class="list-group-item">Book Title: <?php echo $singleItem->title?></li>

  </ul>
</div>

</body>
</html>